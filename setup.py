from setuptools import setup, find_packages

setup(name='windb2',
      version='3.5.1',
      install_requires=['pytz'],
      packages=find_packages(),
)
